package pl.vattenfall.katas.java8streams.s08_partitioningBy;

import java.util.List;
import java.util.Map;

import org.junit.Test;

import pl.vattenfall.katas.java8streams.model.Person.Person;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static pl.vattenfall.katas.java8streams.s08_partitioningBy.Partitioning.partitionAdults;

public class PartitioningSpec {

    @Test
    public void partitionAdultsShouldSeparateKidsFromAdults() {
        Person sara = new Person("Sara", 4);
        Person viktor = new Person("Viktor", 40);
        Person eva = new Person("Eva", 42);
        List<Person> collection = asList(sara, eva, viktor);
        Map<Boolean, List<Person>> result = partitionAdults(collection);
        assertThat(result.get(true)).hasSameElementsAs(asList(viktor, eva));
        assertThat(result.get(false)).hasSameElementsAs(asList(sara));
    }

}