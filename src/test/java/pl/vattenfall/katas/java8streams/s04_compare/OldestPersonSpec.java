package pl.vattenfall.katas.java8streams.s04_compare;

import java.util.List;

import org.junit.Test;

import pl.vattenfall.katas.java8streams.model.Person.Person;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static pl.vattenfall.katas.java8streams.s04_compare.OldestPerson.getOldestPerson;

public class OldestPersonSpec {

    @Test
    public void getOldestPersonShouldReturnOldestPerson() {
        Person sara = new Person("Sara", 4);
        Person viktor = new Person("Viktor", 40);
        Person eva = new Person("Eva", 42);
        List<Person> collection = asList(sara, eva, viktor);
        assertThat(getOldestPerson(collection)).isEqualToComparingFieldByField(eva);
    }

}