package pl.vattenfall.katas.java8streams.s06_filter_map;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.vattenfall.katas.java8streams.model.Person.Person;

class Kids {

    public static Set<String> getKidNames(List<Person> people) {
        Set<String> kids = new HashSet<>();
        for (Person person : people) {
            if (person.getAge() < 18) {
                kids.add(person.getName());
            }
        }
        return kids;
    }

}
