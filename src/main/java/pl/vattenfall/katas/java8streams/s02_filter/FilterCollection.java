package pl.vattenfall.katas.java8streams.s02_filter;

import java.util.ArrayList;
import java.util.List;

class FilterCollection {

    public static List<String> transform(List<String> collection) {
        List<String> newCollection = new ArrayList<>();
        for (String element : collection) {
            if (element.length() < 4) {
                newCollection.add(element);
            }
        }
        return newCollection;
    }


}
