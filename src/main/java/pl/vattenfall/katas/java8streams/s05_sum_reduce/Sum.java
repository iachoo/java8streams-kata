package pl.vattenfall.katas.java8streams.s05_sum_reduce;

import java.util.List;

class Sum {

    public static int calculate(List<Integer> numbers) {
        int total = 0;
        for (int number : numbers) {
            total += number;
        }
        return total;
    }

}
