package pl.vattenfall.katas.java8streams.s01_transform;

import java.util.ArrayList;
import java.util.List;

class ToUpperCase {

    static List<String> transform(List<String> collection) {
        List<String> coll = new ArrayList<>();
        for (String element : collection) {
            coll.add(element.toUpperCase());
        }
        return coll;
    }

}
