package pl.vattenfall.katas.java8streams.s10_joining;

import java.util.List;

import pl.vattenfall.katas.java8streams.model.Person.Person;

class Joining {

    public static String namesToString(List<Person> people) {
        String label = "Names: ";
        StringBuilder sb = new StringBuilder(label);
        for (Person person : people) {
            if (sb.length() > label.length()) {
                sb.append(", ");
            }
            sb.append(person.getName());
        }
        sb.append(".");
        return sb.toString();
    }


}
