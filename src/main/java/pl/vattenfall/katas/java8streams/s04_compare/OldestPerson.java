package pl.vattenfall.katas.java8streams.s04_compare;

import java.util.List;

import pl.vattenfall.katas.java8streams.model.Person.Person;

class OldestPerson {

    public static Person getOldestPerson(List<Person> people) {
        Person oldestPerson = new Person("", 0);
        for (Person person : people) {
            if (person.getAge() > oldestPerson.getAge()) {
                oldestPerson = person;
            }
        }
        return oldestPerson;
    }

}
